package rs.ac.bg.etf.sales.dto;

public class User {
    private int id;

    private String name;
    private boolean enabled;

    public User() {
    }

    public User(String name, boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
