package rs.ac.bg.etf.sales.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rs.ac.bg.etf.sales.dto.Ticket;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Service
public class TicketingServiceAdapter {

    @Value("${ticketing.url}")
    private String ticketingUrl;

    private String uri;
    private RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    void init() {
        uri = ticketingUrl + "/ticket";
    }

    public List<Ticket> getAllTickets() {
        Ticket[] tickets = restTemplate.getForObject(uri, Ticket[].class);

        return Arrays.asList(tickets);
    }

    public List<Ticket> getAvailableTickets() {
        Ticket[] tickets = restTemplate.getForObject(uri + "/available", Ticket[].class);

        return Arrays.asList(tickets);
    }

    public void buyTicket(int id) {
        restTemplate.postForObject(uri + "/buy/" + id, "", String.class);
    }
}
