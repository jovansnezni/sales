package rs.ac.bg.etf.sales.dto;

public class Ticket {
    private int id;
    private String fromCity;
    private String toCity;
    private int amount;
    private int soldAmount;

    public Ticket() {
    }

    public Ticket(String fromCity, String toCity, int amount, int soldAmount) {
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.amount = amount;
        this.soldAmount = soldAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(int soldAmount) {
        this.soldAmount = soldAmount;
    }
}

