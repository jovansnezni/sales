package rs.ac.bg.etf.sales.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.bg.etf.sales.service.PaymentServiceAdapter;
import rs.ac.bg.etf.sales.service.TicketingServiceAdapter;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    TicketingServiceAdapter ticketingService;

    @Autowired
    PaymentServiceAdapter paymentService;

    @GetMapping("buy/{ticketid}/{cardnumber}")
    void buyTicket(@PathVariable int ticketid, @PathVariable String cardnumber) {
        //buy(3, "000-000-000");
        buy(ticketid, cardnumber);
    }

    private void buy(int ticketId, String cardNumber) {
        boolean isTicketingPossible;
        boolean isPaymentPossible;

        isTicketingPossible =
                ticketingService
                        .getAvailableTickets().stream()
                        .anyMatch(ticket -> ticket.getId() == ticketId);

        isPaymentPossible =
                paymentService
                        .getUserForCreditCard(cardNumber)
                        .isEnabled();

        if (isTicketingPossible && isPaymentPossible) {
            ticketingService.buyTicket(ticketId);
            paymentService.withdrawalMoney(cardNumber, 300);//money should be in ticket :-D
        }

    }

}
