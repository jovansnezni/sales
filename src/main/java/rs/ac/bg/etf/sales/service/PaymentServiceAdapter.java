package rs.ac.bg.etf.sales.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rs.ac.bg.etf.sales.dto.User;

import javax.annotation.PostConstruct;

@Service
public class PaymentServiceAdapter {

    @Value("${payment.url}")
    private String paymentUrl;

    private String userUri;
    private String creditCardUri;

    private RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    void init() {
        userUri = paymentUrl + "/user";
        creditCardUri = paymentUrl + "/creditcard";
    }

    public boolean isEnabled(int id) {
        return restTemplate.getForObject(userUri + "/" + id + "/enabled", Boolean.class);
    }

    public User getUserForCreditCard(String number) {
        return restTemplate.getForObject(userUri + "/cardnumber/" + number, User.class);
    }

    public void withdrawalMoney(String cardnumber, int amount) {

        restTemplate.put(creditCardUri + "/" + cardnumber + "/" + amount, "");
    }


}
