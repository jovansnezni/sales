# Sales

Zavisnosti ovih projekata su odrzavane Maven sistemom. Maven funkcionise tako sto u u fajlu pom.xml navodite verzije svih bibliotkea od kojih vas projekat zavisi, i onda Maven za vas skida sve neophodne jarove onih verzija koej ste naveli.

Maven mora biti globalno instaliran na racunaru sto mozete proveriti izvrsavanjem komande `mvn -v`.

Za rad na projektu ukoliko koristite InteliJ prilikom otvaranja projekta InteliJ ce prepoznati da se radi o Maven projektu i na osnovu pom.xml skinuti sve neophodne jarove.

### Objasnjenje pojedinih funkcionalnosti ###

	@Service
	public class TicketingServiceAdapter {
...
	
	@Autowired
    TicketingServiceAdapter ticketingService;

@Service anotacija govori Springu da pri pokretanju aplikacije automatski kreira jedan objekat te klase. Nakon toga toj klasi mozete pristupati iz drugih koriscenjem @Autowierd anotacije.  Pri pokretanju aplikacije Spring prvo kreira sve @Service objekte a onda ih ubaciju i sve klase koje imaju @Autowired tog objekta.  


	public interface TicketRepository extends CrudRepository<Ticket, Integer> {

    @Query("from Ticket t where t.soldAmount < t.amount")
    Iterable<Ticket> findAvailableTickets();
	}

Nasledjivanjem CrudRepositorija dobijate automatski sve CRUD operacije nad tabelom tj entitetom. Entitet prosledjujete kao prvi parametar generika. Dodatno u interfejsu mozete dodavati metode i pisati prozivoljne upite. Ovaj interfejs ce se automatski implementirati prilikom pokretanje aplikacije tako da se moze koristi kao da je inicijalizovan konkretnim objektom. Isto kao i sa @Service klasama i sve sto implementira CrudRepository se kreira pri pokretanju a u drugim klasama ga mozete koristiti sa @Autowired anotacijom.

S obzirom da se prilikom izvrsavanja konstruktora ne mogu koristiti @Autowired stvari, jer objekat se prvo kreira a ankon toga u njega ubacuju zavisnosti, postoji @PostConstruct anotacija koja se izvrsava nakon ubacivanja @Autowired stvari u klasu:

	@PostConstruct
    private void initData() {

### Mikroservisi

Cilj ovog projekta je bio demnostracija kako 3 odvojene aplikacije mogu da komuniciraju medjusobno u cilju obavljanja prodaje jedne karte. Komunikacija se obavlja pomocu Resta.

Neke od prednosti deljenja sistema i razlicite aplikacije su te da na svakoj moze raditi odvojeni tim, brze je pokretanje i testiranja podeljenih aplikacija, lakse je skalirati opterecene delove sistema. 

Takodje prednosti su te da za svaki problem mozete izabrate drugu tehnologiju, na primer ukoliko vam je potreban neki deo sistema koji radi neko masinsko ucenje mozete ga implementirati u Pytonu a sa ostatkom sistema komunicirati preko Rest protokola

	

